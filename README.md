# GITS Indonesia

* Project Name: TEST BE v1.0
* Employer: GITS Indonesia
* Author: Triadi Arifin
* Due Date: May 6th '18
* Bitbucket URL: https://bitbucket.org/triadi2608/test-be-v1.0
* Postman Doc: https://documenter.getpostman.com/view/3005461/test-be-v10/RW1ekdXN

## Installing

A step by step series of examples that tell you have to get a development env running

* ``` npm install ```
* ``` Run query files in server/migrations ```
* ``` Using Redis, if don't have Redis, check server/redis.js file ```

## Running the app

* ``` node server/app ```

## Built With

APP

* [Node.js](https://nodejs.org/en/)
* [NPM](https://www.npmjs.com/)
* [Express](https://expressjs.com/)


RDBMS

* [MySQL](https://www.mysql.com/)


Session

* [Redis](https://redis.io/)


Dependencies

* ``` bluebird ```
* ``` body-parser ```
* ``` dotenv ```
* ``` express ```
* ``` joi ```
* ``` moment ```
* ``` mysql ```
* ``` redis ```
* ``` sha1 ```
* ``` sha256 ```
* ``` statuses ```
