// Load dependencies
const joi = require('joi');

// Load model
const loginModel = require(__models + 'auth/loginModel');

exports.doLogin = (req, res) => {
    rules = joi.object({
        username: joi.string().min(5).max(100).empty('').required(),
        password: joi.string().min(6).max(20).empty('').required()
    }).options({
        stripUnknown: true // Accept other objects
    });

    joi.validate(req.body, rules, (err) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);

        if (err) {
            res.status(400);
            res.json(__appHelper.resJson(400, err.details[0].message, null));
        } else {
            loginModel.doLogin(req).then((result) => {
                res.status(result.code);
                res.json(result);
            });
        }
    });
}
