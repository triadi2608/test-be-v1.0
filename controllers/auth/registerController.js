// Load dependencies
const joi = require('joi');

// Load model
const registerModel = require(__models + 'auth/registerModel');

exports.doRegister = (req, res) => {
    rules = joi.object({
        name: joi.string().max(500).empty('').required(),
        username: joi.string().min(5).max(100).empty('').required(),
        password: joi.string().min(6).max(20).empty('').required(),
        con_pass: joi.any().valid(joi.ref('password'))
    }).options({
        stripUnknown: true // Accept other objects
    });

    joi.validate(req.body, rules, (err) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);

        if (err) {
            res.status(400);
            res.json(__appHelper.resJson(400, err.details[0].message, null));
        } else {
            registerModel.doRegister(req).then((result) => {
                res.status(result.code);
                res.json(result);
            });
        }
    });
}
