// Load dependencies
const joi = require('joi');
const moment = require('moment');

// Load model
const taskModel = require(__models + 'private/taskModel');

exports.getAllTasks = (req, res) => {
    taskModel.getAllTasks(req).then((result) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);
        res.status(result.code);
        res.json(result);
    });
}

exports.storeTask = (req, res) => {
    rules = joi.object({
        name: joi.string().max(500).empty('').required(),
        location: joi.string().max(273).empty('').required(),
        start_time: joi.string().empty('').required(),
        priority: joi.number().integer().min(1).max(5).empty('').required()
    }).options({
        stripUnknown: true // Accept other objects
    });

    joi.validate(req.body, rules, (err) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);

        if (err) {
            res.status(400);
            res.json(__appHelper.resJson(400, err.details[0].message, null));
        } else {
            if (moment(moment().format('YYYY-MM-DD') + ' ' + req.body.start_time, 'YYYY-MM-DD HH:mm:ss', true).isValid() !== true) {
                res.status(400);
                res.json(__appHelper.resJson(200, "\"start_time\" is must be a valid format, HH:mm:ss", null));
            } else {
                taskModel.storeTask(req).then((result) => {
                    res.status(result.code);
                    res.json(result);
                });
            }
        }
    });
}

exports.updateTask = (req, res) => {
    rules = joi.object({
        id: joi.number().integer().empty('').required(),
        name: joi.string().max(500).empty('').required(),
        location: joi.string().max(273).empty('').required(),
        start_time: joi.string().empty('').required(),
        priority: joi.number().integer().min(1).max(5).empty('').required()
    }).options({
        stripUnknown: true // Accept other objects
    });

    joi.validate(req.body, rules, (err) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);

        if (err) {
            res.status(400);
            res.json(__appHelper.resJson(400, err.details[0].message, null));
        } else {
            if (moment(moment().format('YYYY-MM-DD') + ' ' + req.body.start_time, 'YYYY-MM-DD HH:mm:ss', true).isValid() !== true) {
                res.status(400);
                res.json(__appHelper.resJson(200, "\"start_time\" is must be a valid format, HH:mm:ss", null));
            } else {
                taskModel.updateTask(req).then((result) => {
                    res.status(result.code);
                    res.json(result);
                });
            }
        }
    });
}

exports.toggleTask = (req, res) => {
    rules = joi.object({
        id: joi.number().integer().empty('').required(),
        status: joi.string().empty('').required()
    }).options({
        stripUnknown: true // Accept other objects
    });

    joi.validate(req.body, rules, (err) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);

        if (err) {
            res.status(400);
            res.json(__appHelper.resJson(400, err.details[0].message, null));
        } else {
            if (req.body.status !== 'completed' && req.body.status !== 'unfinished') {
                res.status(400);
                res.json(__appHelper.resJson(200, "\"status\" only accepted completed/unfinished", null));
            } else {
                taskModel.toggleTask(req).then((result) => {
                    res.status(result.code);
                    res.json(result);
                });
            }
        }
    });
}
