// Load model
const taskModel = require(__models + 'public/taskModel');

exports.getAllTasks = (req, res) => {
    taskModel.getAllTasks(req).then((result) => {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);
        res.status(result.code);
        res.json(result);
    });
}
