// Load dependecies
const sha1 = require('sha1');
const sha256 = require('sha256');
const status = require('statuses');

exports.resJson = (code, message, data) => {
	res = {
		code: code,
		status: [200, 201, 204].indexOf(code) ? false : true,
		message: message !== null ? message : status[code],
		data: data !== null ? data : 'null'
	}

	return res;
}

exports.inArray = (arr, str) => {
	if (arr.indexOf(str) >= 0) {
		return true;
	} else {
		return false;
	}
}

exports.hash = (str) => {
    return sha256(str);
};

exports.generateToken = (userId) => {
    return sha1('USER_' + userId + '-' + process.env.NODE_ENV);
};
