// Load dependencies
const redis = require(__server + 'redis');

exports.public = (req, res, next) => {
    global.__contentType = req.get('Content-Type');
    global.__acceptLanguage = req.get('Accept-Language');

    if ((!__contentType || !__contentType === undefined) && (!__acceptLanguage || __acceptLanguage === undefined)) {
        res.status(406);
        res.json(__appHelper.resJson(406, 'Unauthorized header such as Content-Type and Accept-Language', null));
    } else if (!__contentType || !__contentType === undefined || __contentType !== 'application/json') {
        res.status(406);
        res.json(__appHelper.resJson(406, 'Unauthorized header such as Content-Type', null));
    } else if (!__acceptLanguage || __acceptLanguage === undefined || __appHelper.inArray(process.env.APP_LANG.split(","), __acceptLanguage) === false) {
        res.status(406);
        res.json(__appHelper.resJson(406, 'Unauthorized header such as Accept-Language', null));
    }

    return next();
}

exports.notAllowed = (req, res) => {
    res.set("Content-Type", __contentType);
    res.set("Accept-Language", __acceptLanguage);
    res.status(405);
    res.json(__appHelper.resJson(405, null, null));
}

exports.private = (req, res, next) => {
    token = req.get('Token');

    if (!token) {
        res.set("Content-Type", __contentType);
        res.set("Accept-Language", __acceptLanguage);
        res.status(401);
        res.json(__appHelper.resJson(401, 'You have no right access, please do authentication with token.', null));
    } else {
        redis.getAsync(token)
        .then((reply) => {
            if (!reply) {
                res.set("Content-Type", __contentType);
                res.set("Accept-Language", __acceptLanguage);
                res.status(401);
                res.json(__appHelper.resJson(401, 'Your token has expired, please relogin.', null));
            } else {
                req.body.session = JSON.parse(reply);

                if (process.env.NODE_ENV === 'DEVELOPMENT') {
                    redis.expire(token, 1800); // Expired in half an hour
                } else {
                    redis.expire(token, 259200); // Expired in 3 days
                }

                return next();
            }
        });
    }
}
