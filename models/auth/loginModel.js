// Load dependencies
const mysql = require(__server + 'mysql');
const redis = require(__server + 'redis');

exports.doLogin = (req) => {
    body = req.body;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                id, \
                name, \
                username, \
                password \
            FROM users \
            WHERE \
                username = '" + body.username + "' \
            GROUP BY id \
            LIMIT 1"
        );

        promise = mysql.queryAsync(sql)
        .then(rows => {
            user = rows[0];

            if (!user) {
                return promise.cancel(
                    resolve(__appHelper.resJson(401, "This account does not exists.", null))
                );
            } else if (__appHelper.hash(body.password) !== user.password) {
                return promise.cancel(
                    resolve(__appHelper.resJson(401, "Wrong password.", null))
                );
            } else {
                token = __appHelper.generateToken(user.id);

                user = {
                    "id": user.id,
                    "name": user.name,
                    "username": user.username
                }

                redis.setAsync(token, JSON.stringify(user));

                if (process.env.NODE_ENV === 'DEVELOPMENT') {
                    redis.expire(token, 1800); // Expired in half an hour
                } else {
                    redis.expire(token, 259200); // Expired in 3 days
                }

                resolve(__appHelper.resJson(200, null, {
                    "token": token,
                    "user": user
                }));
            }
        });
    });
}
