// Load dependencies
const mysql = require(__server + 'mysql');
const redis = require(__server + 'redis');

exports.doRegister = (req) => {
    body = req.body;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                id \
            FROM users \
            WHERE \
                username = '" + body.username + "' \
            GROUP BY id \
            LIMIT 1"
        );

        promise = mysql.queryAsync(sql)
        .then(rows => {
            checkUsers = rows[0];

            if (checkUsers) {
                return promise.cancel(
                    resolve(__appHelper.resJson(409, "This account already exists.", null))
                );
            } else {
                sql = (
                    "INSERT INTO \
                        users \
                    SET \
                        name = '" + body.name + "', \
                        username = '" + body.username + "', \
                        password = '" + __appHelper.hash(body.password) + "'"
                );

                return mysql.queryAsync(sql);
            }
        }).then(result => {
            if (!result.insertId) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                sql = (
                    "SELECT \
                        id, \
                        name, \
                        username \
                    FROM users \
                    WHERE \
                        id = '" + result.insertId + "' \
                    GROUP BY id \
                    LIMIT 1"
                );

                return mysql.queryAsync(sql);
            }
        }).then(rows => {
            newUser = rows[0];

            if (!newUser) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                token = __appHelper.generateToken(newUser.id);

                redis.setAsync(token, JSON.stringify(newUser));

                if (process.env.NODE_ENV === 'DEVELOPMENT') {
                    redis.expire(token, 1800); // Expired in half an hour
                } else {
                    redis.expire(token, 259200); // Expired in 3 days
                }

                resolve(__appHelper.resJson(200, null, {
                    "token": token,
                    "user": newUser
                }));
            }

        });
    });
}
