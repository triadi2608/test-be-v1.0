// Load dependencies
const moment = require('moment');
const mysql = require(__server + 'mysql');

exports.getAllTasks = (req) => {
    param = req.query;
    session = req.body.session;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                tasks.id, \
                tasks.name, \
                tasks.location, \
                tasks.start_time, \
                tasks.priority, \
                tasks.status \
            FROM tasks \
            LEFT JOIN users ON tasks.user_id = users.id AND users.status & 3 \
            WHERE \
                users.username = '" + session.username + "' \
                AND tasks.status & 3 "
        );

        if (param.status) {
            if (param.status === 'completed') {
                sql += ("AND tasks.status = 1 ");
            }

            if (param.status === 'unfinished') {
                sql += ("AND tasks.status = 2 ");
            }
        }

        sql += ("ORDER BY ");

        if (param.sortPriority) {
            if (param.sortPriority.toLowerCase() === 'low-high') {
                sql += ("tasks.priority ASC, ");
            }

            if (param.sortPriority.toLowerCase() === 'high-low') {
                sql += ("tasks.priority DESC, ");
            }
        }

        sql += ("tasks.start_time ASC");

        promise = mysql.queryAsync(sql)
        .then(rows => {
            tasks = rows;

            if (tasks.length) {
                tasks.forEach(function (item) {
                    if (item.status === 1) {
                        item.status = 'completed';
                    }

                    if (item.status === 2) {
                        item.status = 'unfinished';
                    }
                });
            }

            resolve(__appHelper.resJson(200, null, {
                "tasks": tasks
            }));
        });
    });
}

exports.storeTask = (req) => {
    body = req.body;
    session = req.body.session;

    return new Promise((resolve) => {
        sql = (
            "INSERT INTO \
                tasks \
            SET \
                user_id = '" + session.id + "', \
                name = '" + body.name + "', \
                location = '" + body.location + "', \
                start_time = '" + body.start_time + "', \
                priority = '" + body.priority + "'"
        );

        promise = mysql.queryAsync(sql)
        .then(result => {
            if (!result.insertId) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                sql = (
                    "SELECT \
                        id, \
                        name, \
                        location, \
                        start_time, \
                        priority, \
                        status \
                    FROM tasks \
                    WHERE \
                        id = '" + result.insertId + "' \
                        AND status & 3 "
                );

                return mysql.queryAsync(sql);
            }
        }).then(rows => {
            newTask = rows[0];

            if (!newTask) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                newTask.status = 'unfinished';

                resolve(__appHelper.resJson(200, null, {
                    "task": newTask
                }));
            }
        });
    });
}

exports.updateTask = (req) => {
    body = req.body;
    session = req.body.session;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                id \
            FROM tasks \
            WHERE \
                id = '" + body.id + "' \
                AND status & 3 "
        );

        promise = mysql.queryAsync(sql)
        .then(rows => {
            checkTask = rows[0];

            if (!checkTask) {
                return promise.cancel(
                    resolve(__appHelper.resJson(404, "There's no task with this id.", null))
                );
            } else {
                sql = (
                    "UPDATE tasks \
                    SET \
                        name = '" + body.name + "', \
                        location = '" + body.location + "', \
                        start_time = '" + body.start_time + "', \
                        priority = '" + body.priority + "', \
                        updated_at = '" + moment().format('YYYY-MM-DD HH:mm:ss') + "' \
                    WHERE \
                        id = '" + body.id + "'"
                );

                return mysql.queryAsync(sql);
            }
        }).then(result => {
            if (!result.affectedRows) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                sql = (
                    "SELECT \
                        id, \
                        name, \
                        location, \
                        start_time, \
                        priority, \
                        status \
                    FROM tasks \
                    WHERE \
                        id = '" + body.id + "' \
                        AND status & 3 "
                );

                return mysql.queryAsync(sql);

            }
        }).then(rows => {
            updatedTask = rows[0];

            if (!updatedTask) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                if (updatedTask.status === 1) {
                    updatedTask.status = 'completed';
                }

                if (updatedTask.status === 2) {
                    updatedTask.status = 'unfinished';
                }

                resolve(__appHelper.resJson(200, null, {
                    "task": updatedTask
                }));
            }
        });
    });
}

exports.toggleTask = (req) => {
    body = req.body;
    session = req.body.session;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                id \
            FROM tasks \
            WHERE \
                id = '" + body.id + "' \
                AND status & 3 "
        );

        promise = mysql.queryAsync(sql)
        .then(rows => {
            checkTask = rows[0];

            if (!checkTask) {
                return promise.cancel(
                    resolve(__appHelper.resJson(404, "There's no task with this id.", null))
                );
            } else {
                if (body.status === 'completed') {
                    body.status = 1;
                }

                if (body.status === 'unfinished') {
                    body.status = 2;
                }

                sql = (
                    "UPDATE tasks \
                    SET \
                        status = '" + body.status + "', \
                        updated_at = '" + moment().format('YYYY-MM-DD HH:mm:ss') + "' \
                    WHERE \
                        id = '" + body.id + "'"
                );

                return mysql.queryAsync(sql);
            }
        }).then(result => {
            if (!result.affectedRows) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                sql = (
                    "SELECT \
                        id, \
                        name, \
                        location, \
                        start_time, \
                        priority, \
                        status \
                    FROM tasks \
                    WHERE \
                        id = '" + body.id + "' \
                        AND status & 3 "
                );

                return mysql.queryAsync(sql);

            }
        }).then(rows => {
            updatedTask = rows[0];

            if (!updatedTask) {
                return promise.cancel(
                    resolve(__appHelper.resJson(500, null, null))
                );
            } else {
                if (updatedTask.status === 1) {
                    updatedTask.status = 'completed';
                }

                if (updatedTask.status === 2) {
                    updatedTask.status = 'unfinished';
                }

                resolve(__appHelper.resJson(200, null, {
                    "task": updatedTask
                }));
            }
        });
    });
}
