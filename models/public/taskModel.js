// Load dependencies
const mysql = require(__server + 'mysql');

exports.getAllTasks = (req) => {
    param = req.query;

    return new Promise((resolve) => {
        sql = (
            "SELECT \
                tasks.id, \
                users.username, \
                tasks.name, \
                tasks.location, \
                tasks.priority, \
                tasks.start_time, \
                tasks.status \
            FROM tasks \
            LEFT JOIN users ON tasks.user_id = users.id AND users.status & 3 \
            WHERE \
                tasks.status & 3 "
        );

        if (param.username) {
            sql += ("AND users.username = '" + param.username + "' ");
        }

        if (param.status) {
            if (param.status.toLowerCase() === 'completed') {
                sql += ("AND tasks.status = 1 ");
            }

            if (param.status.toLowerCase() === 'unfinished') {
                sql += ("AND tasks.status = 2 ");
            }
        }

        sql += ("ORDER BY ");

        if (param.sortUsername) {
            if (param.sortUsername.toLowerCase() === 'a-z') {
                sql += ("users.username ASC, ");
            }

            if (param.sortUsername.toLowerCase() === 'z-a') {
                sql += ("users.username DESC, ");
            }
        } else {
            sql += ("users.username ASC, ");
        }

        if (param.sortPriority) {
            if (param.sortPriority.toLowerCase() === 'low-high') {
                sql += ("tasks.priority ASC, ");
            }

            if (param.sortPriority.toLowerCase() === 'high-low') {
                sql += ("tasks.priority DESC, ");
            }
        }

        sql += ("tasks.start_time ASC");

        promise = mysql.queryAsync(sql)
        .then(rows => {
            tasks = rows;

            if (tasks.length) {
                tasks.forEach(function (item) {
                    if (item.status === 1) {
                        item.status = 'completed';
                    }

                    if (item.status === 2) {
                        item.status = 'unfinished';
                    }
                });
            }

            resolve(__appHelper.resJson(200, null, {
                "tasks": tasks
            }));
        });
    });
}
