// Load dependencies
const router = require('express').Router();

// Load middlewares
const routeMiddleware = require(__middleware + 'routeMiddleware');

// Load controllers
const loginController = require(__controllers + 'auth/loginController');
const registerController = require(__controllers + 'auth/registerController');

// Define routes
router.route('/register')
	.post(registerController.doRegister)
	.all(routeMiddleware.notAllowed);
router.route('/login')
	.post(loginController.doLogin)
	.all(routeMiddleware.notAllowed);

module.exports = router;
