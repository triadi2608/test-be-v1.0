// Load dependencies
const router = require('express').Router();

// Load middlewares
const routeMiddleware = require(__middleware + 'routeMiddleware');

// Define routes
router.route('/')
	.get((req, res) => {
		var data = {
			"Project Name": "TEST BE v1.0",
			"Employer": "GITS Indonesia",
			"Author": "Triadi Arifin",
			"Due Date": "May 6th '18",
			"Bitbucket URL": "https://bitbucket.org/triadi2608/test-be-v1.0",
			"Postman Doc": "https://documenter.getpostman.com/view/3005461/collection/RW1ekdXN"
		}

		res.set({
			"Content-Type": __contentType,
			"Accept-Language": __acceptLanguage
		});
		res.status(200);
		res.json(__appHelper.resJson(200, null, data));
	})
	.all(routeMiddleware.notAllowed);

router.use('/', require('./authRoute'));
router.use('/user', routeMiddleware.private, require('./privateRoute'));
router.use('/', require('./publicRoute'));

module.exports = router;
