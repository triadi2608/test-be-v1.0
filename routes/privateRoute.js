// Load dependencies
const router = require('express').Router();

// Load middlewares
const routeMiddleware = require(__middleware + 'routeMiddleware');

// Load controllers
const taskController = require(__controllers + 'private/taskController');

// Define routes
router.route('/tasks')
    .get(taskController.getAllTasks)
    .patch(taskController.updateTask)
    .post(taskController.storeTask)
    .all(routeMiddleware.notAllowed);
router.route('/toggle-task')
    .patch(taskController.toggleTask)
    .all(routeMiddleware.notAllowed);

module.exports = router;
