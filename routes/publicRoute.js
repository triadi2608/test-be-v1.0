// Load dependencies
const router = require('express').Router();

// Load middlewares
const routeMiddleware = require(__middleware + 'routeMiddleware');

// Load controllers
const taskController = require(__controllers + 'public/taskController');

// Define routes
router.route('/tasks')
    .get(taskController.getAllTasks)
    .all(routeMiddleware.notAllowed);

module.exports = router;
