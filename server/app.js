// Load dependencies
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const express = require('express');
const app = express();

app.use(bodyParser.json());

// Load .env
dotenv.load();

// Define global variables
global.__controllers = __dirname + '/../controllers/';
global.__helpers = __dirname + '/../helpers/';
global.__middleware = __dirname + '/../middlewares/';
global.__models = __dirname + '/../models/';
global.__routes = __dirname + '/../routes/';
global.__server = __dirname + '/';
global.__appHelper = require(__helpers + 'appHelper');

// Load middlewares
const routeMiddleware = require(__middleware + 'routeMiddleware');

// Load routes
const routes = require(__routes);

app.use('/' + process.env.APP_VERSION, routeMiddleware.public, routes);

app.use((req, res) => {
	res.status(404);
	res.json(__appHelper.resJson(404, 'Route not found.', null));
});

if (process.env.NODE_ENV === 'DEVELOPMENT') {
	app.listen(process.env.NODE_PORT, process.env.NODE_HOST, () => {
		console.log('Environment: "' + process.env.NODE_ENV + '"');
		console.log('Listening on port: ":' + process.env.NODE_PORT + '"');
	});
} else {
	app.listen(process.env.NODE_PORT, () => {
		console.log('Environment: "' + process.env.NODE_ENV + '"');
		console.log('Listening on port: ":' + process.env.NODE_PORT + '"');
	});
}
