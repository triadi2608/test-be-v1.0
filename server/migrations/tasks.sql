SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(10) unsigned NOT NULL,
    `name` text NOT NULL,
    `location` text NOT NULL,
    `start_time` time NOT NULL,
    `priority` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-5 ; Low - High',
    `status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1: Completed, 2: Unfinished, 4: Deleted',
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_tasks-user_id` (`user_id`),
    CONSTRAINT `FK_tasks-user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) COLLATE 'utf8_general_ci';

SET FOREIGN_KEY_CHECKS = 1;
