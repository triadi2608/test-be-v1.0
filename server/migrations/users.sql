SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    `username` varchar(100) NOT NULL,
    `password` char(64) NOT NULL,
    `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: Active, 2: Inactive, 4: Deleted',
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_username_unique` (`username`)
) COLLATE 'utf8_general_ci';

SET FOREIGN_KEY_CHECKS = 1;
