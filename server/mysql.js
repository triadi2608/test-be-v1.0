// Load dependencies
const mysql = require('mysql');
const Promise = require('bluebird');

function connectDatabase() {
    db = mysql.createPool({
        connectionLimit: 100,
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
        dateStrings: 'TIMESTAMP',
        debug: false
    });

    Promise.promisifyAll(db);
    Promise.config({
        cancellation: true
    });

    return db;
}

module.exports = connectDatabase();
