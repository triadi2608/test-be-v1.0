// Load dependencies
const redis = require('redis');
const Promise = require('bluebird');

Promise.promisifyAll(redis);

// If don't have redis, change REDIS_ => REDIS_CLOUD_

host = process.env.REDIS_HOST; // If don't have redis, change REDIS_HOST => REDIS_CLOUD_HOST
pass = process.env.REDIS_PASSWORD; // If don't have redis, change REDIS_PASSWORD => REDIS_CLOUD_PASSWORD

client = redis.createClient(host);

client.auth(pass, (err) => {
    if (err) throw err;
});

client.on('error', function(err) {
    if (err) throw err;
});

module.exports = client;
